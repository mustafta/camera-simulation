/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package DigitalCameras;
public abstract class DigitalCamera {
    public String make;
    public String model;
    public double megapixels;
    public String internalmemorySize;
    public String externalmemorySize;
    
    public DigitalCamera (String make, String model, double megapixels, String internalmemorySize, String externalmemorySize){
    
    
    }
    
    
     public abstract String getMake();
     public abstract String getModel();

     public abstract double getMegapixels();
     public abstract String getInternalmemorySize();
     public abstract String getExternalmemorySize();
    
    public abstract String describeCamera();
}
