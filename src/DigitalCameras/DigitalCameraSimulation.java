/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package DigitalCameras;

import java.util.ArrayList;
import java.util.List;

public class DigitalCameraSimulation {

    public static void main(String[] args) {
        PointAndShootCamera camera= new PointAndShootCamera ("Canon", "PowershotA590", 8.0, "None", "16GB");
        PhoneCamera cameraphone= new PhoneCamera ("Apple", "iPhone", 6.0, "64GB", "None");
        
         
                System.out.println(camera.describeCamera());
                System.out.println(cameraphone.describeCamera());
                
    }
    
}