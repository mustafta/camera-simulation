/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package DigitalCameras;
public class PhoneCamera extends DigitalCamera{
    public String make;
    public String model;
    public double megapixels;
    public String internalmemorySize;
    public String externalmemorySize;
    
     
    public PhoneCamera (String make, String model, double megapixels, String internalmemorySize, String externalmemorySize) {
        super(make, model, megapixels, internalmemorySize, externalmemorySize);
        this.make = make;
        this.model = model;
        this.megapixels = megapixels;
        this.internalmemorySize = internalmemorySize;
        this.externalmemorySize = externalmemorySize;
    }

    @Override
    public String getMake() {
        return make;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public double getMegapixels() {
        return megapixels;
    }

    @Override
    public String getInternalmemorySize() {
        return internalmemorySize;
    }

    @Override
    public String getExternalmemorySize() {
        return externalmemorySize;
    }
    
    
    
    @Override
    public String describeCamera(){
    
    return "Make " + getMake() + " \t " + " Model " + getModel() + " \t " + " Resolution " + getMegapixels() + " \t " + " Internal Memory Size " + getInternalmemorySize() + " \t "+ "External Memry Size " + getExternalmemorySize();
    }    
    
}
